import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  MenuController,
  NavController,
  LoadingController,
  AlertController
} from '@ionic/angular';
//PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
// SERVICES
import {
  InformationService
} from '../../services/information/information.service';


@Component({
  selector: 'app-detallenhome',
  templateUrl: './detallenhome.page.html',
  styleUrls: ['./detallenhome.page.scss'],
})
export class DetallenhomePage implements OnInit {
  typeNetwork: string;
  public user: Object;
  public status: boolean;

  idResidencia: any;
  data: any;
  detalles: any;

  // DATA
  titulo_noticia : string;
  noticia : string;
  fecha_noticia : any;
  archivo_noticia : string;

  verArchivo : boolean = false;

  constructor(
    public router: Router,
    private network: Network,
    private storage: Storage,
    public loadingController: LoadingController,
    private alertCtrl: AlertController,
    private _infoService: InformationService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
      }
    });

    this.handleGetStorageUser();
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.idResidencia = user[0].id_residencia;
        if (this.idResidencia) {
          this.handleGetDetelle();
        } else {
          console.log('error faltan campos de el local storage')
        }
      })
  };

  async handleGetDetelle() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'residencia': this.idResidencia,
        'id_noticia': this.data
      };
      await this._infoService.handleDetalleNoticias(data)
        .subscribe(res => {
          this.detalles = res;
          console.log(this.detalles);

          this.titulo_noticia = this.detalles[0].titulo_noticia;
          this.noticia = this.detalles[0].noticia;
          let fecha = new Date(this.detalles[0].fecha_noticia * 1000)
          let dia = fecha.getDate(); //Tenemos 2050
          let mes = (fecha.getUTCMonth() + 1); //Tenemos 2050
          let año = fecha.getUTCFullYear(); //Tenemos 2050

          let date = dia + '/' + mes + '/' + año;

          this.fecha_noticia = date;

          this.archivo_noticia = this.detalles[0].archivo_noticia;

          if (this.archivo_noticia != ""){
            this.verArchivo = true;
          } 


        }, err => {
          console.log(err);
          this.status = false;
          this.alertMsj(
            "Oh, algo salio mal",
            "Intenta otra vez."
          );

        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

}