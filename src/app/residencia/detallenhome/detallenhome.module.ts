import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetallenhomePageRoutingModule } from './detallenhome-routing.module';

import { DetallenhomePage } from './detallenhome.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetallenhomePageRoutingModule
  ],
  declarations: [DetallenhomePage]
})
export class DetallenhomePageModule {}
