import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetallenhomePage } from './detallenhome.page';

describe('DetallenhomePage', () => {
  let component: DetallenhomePage;
  let fixture: ComponentFixture<DetallenhomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallenhomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetallenhomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
