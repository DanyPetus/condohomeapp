import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetallenhomePage } from './detallenhome.page';

const routes: Routes = [
  {
    path: '',
    component: DetallenhomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetallenhomePageRoutingModule {}
