import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from '@angular/router';
import {
  MenuController,
  NavController,
  LoadingController,
  AlertController
} from '@ionic/angular';
//PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
// SERVICES
import {
  InformationService
} from '../../services/information/information.service';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.page.html',
  styleUrls: ['./contacto.page.scss'],
})
export class ContactoPage implements OnInit {
  // Variables a usar
  typeNetwork: string;
  public user: Object;
  public status: boolean;
  idResidencia: string;

  // Datos del admin
  datos_bancarios: string;
  email_administrador: string;
  nombre_administrador: string;
  nombre_complejo: string;
  telefono_administrador: string;
  administrador: any;

  constructor(
    public router: Router,
    private network: Network,
    private storage: Storage,
    public loadingController: LoadingController,
    private alertCtrl: AlertController,
    private _infoService: InformationService,
  ) {}

  ngOnInit() {
    this.handleGetStorageUser();
  }

  async handleGetStorageUser() {
    this.storage.get("user").then(
      user => {
        this.idResidencia = user[0].id_residencia;
        if (this.idResidencia) {
          this.handleGetAdmin();
        } else {
          console.log('error faltan campos de el local storage')
        }
      })
  };

  async handleGetAdmin() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      let data = {
        'residencia': this.idResidencia
      };
      await this._infoService.handleGetAdmin(data)
        .subscribe(res => {
          this.administrador = res;
          // console.log(this.administrador);

          this.datos_bancarios = this.administrador[0].datos_bancarios;
          this.email_administrador = this.administrador[0].email_administrador;
          this.nombre_administrador = this.administrador[0].nombre_administrador;
          this.nombre_complejo = this.administrador[0].nombre_complejo;
          this.telefono_administrador = this.administrador[0].telefono_administrador;


        }, err => {
          console.log(err);
          this.status = false;
          this.alertMsj(
            "Oh, algo salio mal",
            "Intenta otra vez."
          );

        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

}