import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  Router,
  NavigationExtras
} from '@angular/router';
import {
  MenuController,
  NavController,
  LoadingController,
  AlertController
} from '@ionic/angular';
//PLUGINS
import {
  Network
} from '@ionic-native/network/ngx';
import {
  Storage
} from '@ionic/storage';
// SERVICES
import {
  InformationService
} from '../../services/information/information.service';
import {
  IonSearchbar
} from '@ionic/angular';


@Component({
  selector: 'app-anuncios',
  templateUrl: './anuncios.page.html',
  styleUrls: ['./anuncios.page.scss'],
})
export class AnunciosPage implements OnInit {

  @ViewChild('search', {
    static: false
  }) search: IonSearchbar;

  typeNetwork: string;
  public user: Object;
  public status: boolean;

  anuncios: any;
  listados = [];
  listadosFilter: any;;
  private searchedItem: any;


  constructor(
    public router: Router,
    private network: Network,
    private storage: Storage,
    public loadingController: LoadingController,
    private alertCtrl: AlertController,
    private _infoService: InformationService,
  ) {
    this.handleGetPay();
  }

  ngOnInit() {}

  // let dd = String(today.getDate()).padStart(2, '0');
  // let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  // let yyyy = today.getFullYear();

  // this.today = dd + '/' + mm + '/' + yyyy;

  async handleGetPay() {
    this.typeNetwork = this.network.type;
    if (this.typeNetwork != 'none') {
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        mode: 'ios',
        duration: 2000
      });
      await loading.present();
      await this._infoService.handleGetAnuncios()
        .subscribe(res => {
          this.anuncios = res;
          console.log(this.anuncios);

          this.anuncios.forEach(element => {
            let id = element.id_noticia;
            let noticia = element.noticia
            let titulo_noticia = element.titulo_noticia
            let fecha = new Date(element.fecha_noticia * 1000)
            let dia = fecha.getDate(); //Tenemos 2050
            let mes = (fecha.getUTCMonth() + 1); //Tenemos 2050
            let año = fecha.getUTCFullYear(); //Tenemos 2050


            let date = dia + '/' + mes + '/' + año;
            // console.log(date)

            let arrayCalendar = {
              id,
              titulo_noticia,
              noticia,
              date
            }

            this.listados.push(arrayCalendar)
            this.searchedItem = this.listados;

          });
          this.listadosFilter = this.listados;
          loading.dismiss();

        }, err => {
          console.log(err);
          this.status = false;
          this.alertMsj(
            "Oh, algo salio mal",
            "Intenta otra vez."
          );
          loading.dismiss();

        });
    } else {
      this.alertMsj(
        "Error de conexión",
        "Verifica tu conexión a internet."
      );
    }
  }

  buscar(ev) {
    let val = ev.detail.value;

    if (val && val.trim() !== '') {
      this.listadosFilter = this.listados.filter(term =>
        term.titulo_noticia.toLowerCase().indexOf(val.toLowerCase().trim()) > -1)
    } else {
      this.listadosFilter = this.listados;
    }
  }

  goTo(id) {
    // console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
    // Data que enviaremos
    let data = id
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['detallenhome'], navigationExtras);
  }

  async alertMsj(header: string, message: string) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['Ok'],
      mode: 'ios'
    });
    alert.present();
  }

}