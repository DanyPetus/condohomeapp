import {
  Observable,
  throwError
} from 'rxjs';

import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';

import {
  catchError,
  map
} from 'rxjs/operators';

import {
  Injectable
} from '@angular/core';

import {
  urlProd
} from '../index.url';

import {
  Info
} from '../index.endpoints';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    public http: HttpClient
  ) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error has been occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }


  handleLogin(data): Observable < any > {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    console.log(data);
    let apiUrl = `${urlProd + Info.login + '&email=' + data.email + '&password=' + data.password} `;
    console.log(apiUrl);
    return this.http.get(apiUrl, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

}
